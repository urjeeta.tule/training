﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TrainingDBEntities db = new TrainingDBEntities();

            var prd = db.Products.Where(p => p.Id == 11).SingleOrDefault();

            db.Products.Remove(prd);
            //prd.Price += 100;

            //Product prd = new Product { Name = "Tennis Ball", Description = "For child", Category = "Tennis", Price = 199 };
            //db.Products.Add(prd);
            db.SaveChanges();

            foreach (var item in db.Products.Where(p => p.Price > 30))
            {
                Console.WriteLine("ID: {0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice: {4}", item.Id, item.Name, item.Description, item.Category, item.Price);
            }
        }
    }
}
