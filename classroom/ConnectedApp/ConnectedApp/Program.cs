﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConnectedApp
{
    class Program
    {
        static void Main(string[] args)
        {
            
                Console.WriteLine("Enter Name,Description,Category,Price");
                string[] ans = Console.ReadLine().Split(' ');
                string name = ans[0];
                string desc = ans[1];
                string cat = ans[2];
                double p = Convert.ToDouble(ans[3]);

            string insertCommand = "insert into Products(Name,Description,Category,Price) values('" + name + "','" + desc + "','" + cat + "','" + p + "')";

            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            {
                //open connection with database
                conn.Open();

                //create required command
                SqlCommand command = new SqlCommand();

                command.CommandText = insertCommand;
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;

                var result = command.ExecuteNonQuery();

                if (result > 0)
                {
                    Console.WriteLine("Product inserted!");
                }

                command.CommandText = "select * from Products";
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;

                //execute command to get data from database
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows == false)
                    Console.WriteLine("Product with given ID doesn't exist");

                //use data from reader object
                while (reader.Read())
                {
                    Console.WriteLine(string.Format("ID: {0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice: {4}", reader["Id"], reader["Name"], reader["Description"], reader["Category"], reader["Price"]));
                    Console.WriteLine();
                }

                reader.Close();
                conn.Close();

                Console.ReadLine();
            }
        }
    }
}
