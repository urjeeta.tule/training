﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DisconnectedApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("select * from Products", conn);

                DataSet ds = new DataSet();

                adapter.Fill(ds,"Products");
                

                Console.WriteLine("Table before adding");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Console.WriteLine("row " + i + " contains ");
                    Console.WriteLine(ds.Tables[0].Rows[i]["Name"] + "\t" + ds.Tables[0].Rows[i]["Description"] + "\t" + ds.Tables[0].Rows[i]["Category"] + "\t" + ds.Tables[0].Rows[i]["Price"]);
                }

                DataRow row = ds.Tables[0].NewRow();
                row["Name"] = "Tennis Ball";
                row["Description"] = "to play tennis duh";
                row["Category"] = "Tennis";
                row["Price"] = 23.4;

                ds.Tables[0].Rows.Add(row);

                SqlCommandBuilder builder = new SqlCommandBuilder(adapter);

                // add rows to dataset

                builder.GetInsertCommand();
                adapter.Update(ds, "Products");

                Console.WriteLine("Table after adding");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Console.WriteLine("row " + i + " contains ");
                    Console.WriteLine(ds.Tables[0].Rows[i]["Name"] + "\t" + ds.Tables[0].Rows[i]["Description"] + "\t" + ds.Tables[0].Rows[i]["Category"] + "\t" + ds.Tables[0].Rows[i]["Price"]);
                }

                Console.ReadLine();
            }
        }
    }
}
