﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MyDetails md = new MyDetails { FirstName = "Urjeeta", LastName = "Tule" };
            Printer.Print(md);

            Console.ReadLine();
        }
    }
}
