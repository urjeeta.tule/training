﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApp
{
    [Print("Console")]
    public class MyDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }

    //[Print("ConsoleWindow")]
    //public class OtherDetails
    //{
    //    public string City { get; set; }
    //    public string State { get; set; }
    //}
}
