﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraDelegateApp
{
    public delegate void LoginDelegate(LoginEventArgs l);

    public class Login
    {
        public event LoginDelegate LoginSuccessfull;
        public event LoginDelegate LoginFail;


        public static int Authenticate(User u)
        {
            if (u.username == "flight" && u.password == "flight")
            {
                Console.WriteLine("Authentication success");
                return 1;
            }
            else
            {
                Console.WriteLine("Authentication fail");
                return 0;
            }
        }

        public void LoginResult(User u)
        {
            LoginEventArgs l = new LoginEventArgs();
            l.username = u.username;
            l.password = u.password;

            if (Authenticate(u) == 1)
            {
                LoginSuccessfull(l);
            }
            else
            {
                LoginFail(l);
            }
        }
    }
}
