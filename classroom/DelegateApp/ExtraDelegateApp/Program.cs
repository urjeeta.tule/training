﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraDelegateApp
{
    public class Program
    {
        
        public static void Main(string[] args)
        {
            User u1 = new User();
            User u2 = new User();
            u1.username = "flight";
            u1.password = "flight";
            u2.username = "sgwg";
            u2.password = "sdgg";

            Login login = new Login();
            login.LoginSuccessfull += OnSuccess;
            login.LoginFail += OnFail;
        
            login.LoginResult(u1);
            login.LoginResult(u2);

        }

        

        public static void OnSuccess(LoginEventArgs l)
        {
            Console.WriteLine("Username logged in: {0}", l.username);
            Console.WriteLine("Password logged in: {0}", l.password);
            Console.WriteLine("Login successful!");
        }

        public static void OnFail(LoginEventArgs l)
        {
            Console.WriteLine("Username logged in: {0}", l.username);
            Console.WriteLine("Password logged in: {0}", l.password);
            Console.WriteLine("Login Failed!");
        }

    }
}
