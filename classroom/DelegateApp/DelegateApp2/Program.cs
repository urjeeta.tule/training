﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            MyButton b = new MyButton();
            b.Click += delegate ()
            {
                Console.WriteLine("MyButton clicked!");
            };
            b.RaiseEvent();
        }
    }

    delegate void ClickHandler();

    class MyButton
    {
        public event ClickHandler Click;
        public void RaiseEvent()
        {
            Click();
        }
    }
}
