﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp
{
    //delegate int Compute(int a);
    delegate void Compute(int a);
    delegate void computeEmp(Employee e);


    class Program
    {
        static void Main(string[] args)
        {
            Compute c = new Compute(Square);
            c += Cube;
            //Compute c = Square; C# 3.0 onwards we can call like this
            //var result = c.Invoke(13);
            c.Invoke(13);
            //Console.WriteLine("Result is: {0}", result);

            Console.ReadLine();
        }

        //static int square(int x)
        //{
        //    return (x * x);
        //}

        static void Square(int x)
        {
            Console.WriteLine("Square of {0} is = {1}",x,(x*x));
        }

        static void Cube(int x)
        {
            Console.WriteLine("Cube of {0} is = {1}",x,(x*x*x));
        }
    }
}
