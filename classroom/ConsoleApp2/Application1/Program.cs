﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application1
{
    class Program
    {
        static List<Product> products;
        static int prdId = 0;

        static void Main(string[] args)
        {
            products = new List<Product>();

            bool choice = true;
            Console.WriteLine("Enter 'end' to exit");
            while(choice)
            {
                Console.WriteLine("Enter name:");
                string val = Console.ReadLine();
                if (val == "end")
                {
                    choice = false;
                    break;
                }
                else
                {
                    products.Add(new Product { ID = (prdId + 1), name = val });
                    prdId += 1;
                }
            }

            foreach (var item in products)
            {
                Console.WriteLine(item);
            }
        }
    }
}
