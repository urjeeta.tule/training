﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application1
{
    class Product
    {
        public int ID { get; set; }
        public string name { get; set; }

        public override string ToString()
        {
            return string.Format("ID: {0}\tName: {1}",ID,name);
        }
    }
}
