﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ArrayListTest
{
    class Program
    {
        private static readonly string[] colors = { "MAGENTA", "RED", "WHITE", "BLUE", "CYAN" };
        private static readonly string[] removeColors = { "RED", "WHITE", "BLUE" };

        static void Main(string[] args)
        {
            ArrayList list = new ArrayList(1);
            //add elements from static array into this list
            Console.WriteLine("\nSize: {0}; Capacity: {1}", list.Count, list.Capacity);

            foreach (var color in colors)
            {
                list.Add(color);
            }

            Console.WriteLine("\nSize: {0}; Capacity: {1}", list.Count, list.Capacity);

            ArrayList removeList = new ArrayList(removeColors);
            Console.WriteLine("Arraylist: ");
            DisplayInfo(list);

            removeColorss(list, removeList);
            Console.WriteLine("\nArrayList after removing colors: ");
            DisplayInfo(list);
            Console.ReadLine();
        }

        private static void removeColorss(ArrayList list, ArrayList removeList)
        {
            for (int count = 0; count < removeList.Count; count++)
            {
                list.Remove(removeList[count]);
            }
        }

        private static void DisplayInfo(ArrayList list)
        {
            //iterate through list
            foreach (var item in list)
            {
                Console.Write("{0} ",item);
            }
            Console.WriteLine("\nSize: {0}; Capacity: {1}",list.Count,list.Capacity);

            int index = list.IndexOf("BLUE");
            if (index != 1)
            {
                Console.WriteLine("The ArrayList contains BLUE at index {0}",index);
            }
            else
            {
                Console.WriteLine("The ArrayList does not contain BLUE");
            }

        }
    }
}
