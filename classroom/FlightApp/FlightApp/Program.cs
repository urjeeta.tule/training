﻿using System; 
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string[,] aidata;
            string[,] indata;
            string[,] jadata;

            Console.WriteLine("Air India details are:");
            using (FileStream fs = File.OpenRead(@"C:\training\classroom\FlightApp\FlightApp\AirIndia.txt"))
            {
                StreamReader reader = new StreamReader(fs);
                aidata = getFile(reader,1);
                reader.Close();
                fs.Close();
            }

            Console.WriteLine("\nIndiGo details are:");
            using (FileStream fs = File.OpenRead(@"C:\training\classroom\FlightApp\FlightApp\IndiGo.txt"))
            {
                StreamReader reader = new StreamReader(fs);
                indata = getFile(reader,2);
                reader.Close();
                fs.Close();
            }
        
            Console.WriteLine("\nJet Airways details are:");
            using (FileStream fs = File.OpenRead(@"C:\training\classroom\FlightApp\FlightApp\JetAirways.txt"))
            {
                StreamReader reader = new StreamReader(fs);
                jadata = getFile(reader,2);
                reader.Close();
                fs.Close();
            }

            using (SqlConnection sql = new SqlConnection(@"Data Source = (localdb)\MSSQLLocalDB; Initial Catalog = TrainingDB; Integrated Security = True; Pooling = False"))
            {
                sql.Open();

                //var commandstr = "drop table AirIndia";
                //using (SqlCommand command = new SqlCommand(commandstr, sql))
                //{
                //    command.ExecuteNonQuery();
                //}

                //commandstr = "drop table IndiGo";
                //using (SqlCommand command = new SqlCommand(commandstr, sql))
                //{
                //    command.ExecuteNonQuery();
                //}

                //commandstr = "drop table JetAirways";
                //using (SqlCommand command = new SqlCommand(commandstr, sql))
                //{
                //    command.ExecuteNonQuery();
                //}

                var commandstr = "If not exists (select name from sysobjects where name = 'AirIndia') create table AirIndia(Origin varchar(50),Departure_time varchar(50), Destination varchar(50), Destination_time varchar(50), Price varchar(50))";
                using (SqlCommand command = new SqlCommand(commandstr, sql))
                {
                    command.ExecuteNonQuery();
                }

                var commandstr1 = "If not exists (select name from sysobjects where name = 'IndiGo') create table IndiGo(Origin varchar(50),Departure_time varchar(50), Destination varchar(50), Destination_time varchar(50), Price varchar(50))";
                using (SqlCommand command = new SqlCommand(commandstr1, sql))
                {
                    command.ExecuteNonQuery();
                }

                var commandstr2 = "If not exists (select name from sysobjects where name = 'JetAirways') create table JetAirways(Origin varchar(50),Departure_time varchar(50), Destination varchar(50), Destination_time varchar(50), Price varchar(50))";
                using (SqlCommand command = new SqlCommand(commandstr2, sql))
                {
                    command.ExecuteNonQuery();
                }

                for (int i = 0; i < aidata.GetLength(0); i++)
                {
                    string cend = "";
                    for (int j = 0; j < aidata.GetLength(1); j++)
                    {
                        if (j == (aidata.GetLength(1) - 1))
                            cend += aidata[i, j] + "')";
                        else
                            cend += aidata[i, j] + "','";
                    }

                    var commandstr3 = "insert into AirIndia(Origin,Departure_time,Destination,Destination_time,Price) values ('" + cend;
                    using (SqlCommand command = new SqlCommand(commandstr3, sql))
                    {
                        var result = command.ExecuteNonQuery();
                        Console.WriteLine(result + " for airindia");
                    }
                }



                for (int i = 0; i < indata.GetLength(0); i++)
                {
                    string cend = "";
                    for (int j = 0; j < indata.GetLength(1); j++)
                    {
                        if (j == (indata.GetLength(1) - 1))
                            cend += indata[i, j] + "')";
                        else
                            cend += indata[i, j] + "','";
                    }


                    var commandstr4 = "insert into IndiGo(Origin,Departure_time,Destination,Destination_time,Price) values ('" + cend;
                    using (SqlCommand command = new SqlCommand(commandstr4, sql))
                    {
                        var result = command.ExecuteNonQuery();
                        Console.WriteLine(result + " for indigo");
                    }
                }

                for (int i = 0; i < jadata.GetLength(0); i++)
                {
                    string cend = "";
                    for (int j = 0; j < jadata.GetLength(1); j++)
                    {
                        if (j == (jadata.GetLength(1) - 1))
                            cend += jadata[i, j] + "')";
                        else
                            cend += jadata[i, j] + "','";
                    }

                    var commandstr5 = "insert into JetAirways(Origin,Departure_time,Destination,Destination_time,Price) values ('" + cend;
                    using (SqlCommand command = new SqlCommand(commandstr5, sql))
                    {
                        var result = command.ExecuteNonQuery();
                        Console.WriteLine(result + " for jetairways");
                    }
                }

                sql.Close();
            }

            
            
        }

        public static string[,] getFile(StreamReader r, int ch)
        {
            string[,] filedata = new string[1000, 5];

            string[] lines = new string[1000];
            int j = 0;
            while (!r.EndOfStream)
            {
                string[] ans = new string[5];
                lines[j] = r.ReadLine();
                if (ch == 1)
                { ans = lines[j].Split('|'); }
                else if (ch == 2)
                {
                    ans = lines[j].Split(',');
                }

                filedata[j, 0] = ans[0];
                filedata[j, 1] = ans[1];
                filedata[j, 2] = ans[2];
                filedata[j, 3] = ans[3];
                filedata[j, 4] = ans[4];

                j++;

            }

            for (int i = 0; i < j; i++)
            {
                for (int k = 0; k < 5; k++)
                {
                    Console.Write(filedata[i, k] + "\t");
                }
                Console.WriteLine();
            }

            return filedata;
        }




    }
}
