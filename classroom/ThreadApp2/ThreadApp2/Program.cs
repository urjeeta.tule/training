﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp2
{
    public class Program
    {
        static void Main(string[] args)
        {

            TickTock tt = new TickTock();

            MyThread t1 = new MyThread("Tick", tt);
            MyThread t2 = new MyThread("Tock", tt);

            t1.t.Join();
            t2.t.Join();

            Console.WriteLine("Clock stopped....");

            //4/7/18 code after this comment

            //Printer p = new Printer();

            //ParameterizedThreadStart ps = p.Print;

            //Thread t1 = new Thread(ps);
            //Thread t2 = new Thread(ps);
            //Thread t3 = new Thread(ps);

            //t1.Start("Welcome");
            //t2.Start("To");
            //t3.Start("Tripstack");

            //t1.Join();
            //t2.Join();
            //t3.Join();


            //3/7/18 code after this comment

            //Thread t = new Thread(new ThreadStart(Test.dosomething));

            //t.Start();

            //Thread.Sleep(1000);
            //t.Abort();
            //t.Start();
            //t.Join();
            //Console.WriteLine("Done......");


            //Console.WriteLine("Main() on thread no: {0}",Thread.CurrentThread.ManagedThreadId);

            //Printer a = new Printer('.', 10,"a");
            //Printer b = new Printer('*', 50,"b");

            //Thread t1 = new Thread(new ThreadStart(a.Print));
            //Thread t2 = new Thread(new ThreadStart(b.Print));
            //Console.WriteLine("Priority of t1: {0}",t1.Priority);
            //Console.WriteLine("Priority of t2: {0}", t2.Priority);

            //t1.Start();
            //t2.Start();
            //Console.WriteLine("Main() is waiting for child to finish...");
            //t1.Join();
            //t2.Join();

            //Console.WriteLine("Main() completed...");



            Console.ReadLine();
        }
    }
}
