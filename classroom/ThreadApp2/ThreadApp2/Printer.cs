﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp2
{
    public class Printer
    {
        char ch;
        int sleeptime;
        string objName;

        //public Printer(char c,int t,string n)
        //{
        //    this.ch = c;
        //    this.sleeptime = t;
        //    this.objName = n;
        //}

        public void Print(object m)
        {
            //acquire a lock
            Monitor.Enter(this);
            try
            {

                Console.Write("***  "+ m);
    

                Thread.Sleep(5000);
                Console.WriteLine(" ***");


            }
            catch (Exception)
            { 
                //exception handling code here
            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        //public void Print()
        //{
        //    Console.WriteLine("Print() of {0} is on Thread no. {1}",this.objName,Thread.CurrentThread.ManagedThreadId);
        //    for (int i = 0; i < 100; i++)
        //    {
        //        Console.WriteLine(ch);
        //        Thread.Sleep(sleeptime);

        //    }
        //}
    }
}
