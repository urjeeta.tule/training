﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp2
{
    public class TickTock
    {
        public void Tick(bool running)
        {
            lock (this)
            {
                if (!running)
                {
                    //notify any waiting threads
                    Monitor.Pulse(this);
                    return;
                }

                Console.WriteLine("Tick");

                //let Tock() run
                Monitor.Pulse(this);

                //wait Tock() to complete or notify
                Monitor.Wait(this);
            }
            
        }

        public void Tock(bool running)
        {
            lock (this)
            {
                if (!running)
                {
                    //notify any waiting threads
                    Monitor.Pulse(this);
                    return;
                }

                Console.WriteLine("Tock");

                //let Tock() run
                Monitor.Pulse(this);

                //wait Tock() to complete or notify
                Monitor.Wait(this);
            }
        }
    }
}
