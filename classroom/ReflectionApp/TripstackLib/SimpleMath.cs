﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TripstackLib
{
    public class SimpleMath
    {
        public int Square(int x)
        {
            return x * x;
        }

        public int Cube(int x)
        {
            return x * x * x;
        }
    }
}
