﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryWriterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (FileStream fs = File.OpenRead(@"C:\training\products.txt"))
            {
                BinaryReader reader = new BinaryReader(fs);

                Console.WriteLine("Product ID: {0}", reader.ReadInt32());
                Console.WriteLine("Product Name: {0}", reader.ReadString());
                Console.WriteLine("Product Price: {0}", reader.ReadDouble());

                reader.Close();
                fs.Close();

            }

            Console.ReadLine();






            ////Open or create a file
            //FileStream fs = File.OpenWrite(@"C:\training\products.txt");

            //BinaryWriter writer = new BinaryWriter(fs);

            ////product id
            //writer.Write(123);

            ////product name as string
            //writer.Write("Product 1");

            ////product price as double 
            //writer.Write(34.55);

            ////close writer stream
            //writer.Close();
            //fs.Close();

            //Console.WriteLine("Product details are written to a file");
            //Console.ReadLine();
        }
    }
}
