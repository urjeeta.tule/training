﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIOApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //which directory info you want
            DirectoryInfo di = new DirectoryInfo(@"C:\Windows");

            Console.WriteLine("Listing contents of {0} directory: ",di.FullName);

            //List details of each subdirectory
            foreach (DirectoryInfo d in di.GetDirectories())
            {
                foreach (FileInfo file in d.GetFiles())
                {
                    //List details of every file
                    Console.WriteLine("File Name: " + file.Name);
                    Console.WriteLine("File size in (bytes): " + file.Length);
                    Console.WriteLine("Creation Time: " + file.CreationTime);
                    Console.WriteLine();
                }
            }

            Console.ReadLine();
        }
    }
}
