﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Account a = new Account(5000);
                Console.WriteLine("Current Balance: {0}", a.Balance);

                a.Deposit(1000);
                Console.WriteLine("Current Balance after {0} deposited: {1}", 1000, a.Balance);

                a.Withdraw(10000);
                Console.WriteLine("Current Balance after {0} withdrawn: {1}", 10000, a.Balance);

            }
            catch (InsufficientBalanceException ibe)
            {
                Console.WriteLine(ibe.Message);
            }
        }
    }
}
