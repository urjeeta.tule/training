﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraApplication_fileio
{
    [Serializable]
    public class Department
    {
        public int dep_id;
        public string dep_name;
        public string location;

        public Department()
        {
            //dep_id = -9999;
            dep_name = "";
            location = "";
        }

        public Department(int i, string d, string l)
        {
            dep_id = i;
            dep_name = d;
            location = l;
        }

        public void Print()
        {
            Console.WriteLine("Department Details are: ");
            Console.WriteLine("Department ID:\t" + dep_id);
            Console.WriteLine("Department Name:\t" + dep_name);
            Console.WriteLine("Location:\t" + location);
        }

        public override string ToString()
        {
            return string.Format("ID: {0}\tName: {1}\tLocation: {2}", this.dep_id, this.dep_name, this.location);
        }
    }
}