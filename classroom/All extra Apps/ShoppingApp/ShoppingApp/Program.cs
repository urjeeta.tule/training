﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    public class Program
    {
        
        static void Main(string[] args)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            using (SqlConnection sql = new SqlConnection(@"Data Source = (localdb)\MSSQLLocalDB; Initial Catalog = TrainingDB; Integrated Security = True; Pooling = False; MultipleActiveResultSets=true"))
            {
                sql.Open();

                SqlCommand command = new SqlCommand();

                command.CommandType = System.Data.CommandType.Text;
                command.Connection = sql;
                
                int loop = 1;
                int flag = Execution.CheckIfCartEmpty(command);

                while (loop > 0)
                {
                    menu(flag);

                    string ch = Console.ReadLine();

                    try
                    {
                        switch (ch)
                        {
                            case "1":
                                Execution.ShowAllProducts(command);
                                clearIt();
                                break;
                            case "2":
                                Execution.Categorywise(command, sql);
                                clearIt();
                                break;
                            case "3":
                                Execution.AddProduct(command, sql);
                                clearIt();
                                Execution.CheckIfCartEmpty(command);
                                break;
                            case "5":
                                Execution.ShowCartDetails(command);
                                clearIt();
                                break;
                            case "6":
                                Execution.AddQuantity(command);
                                clearIt();
                                break;
                            case "7":
                                Execution.RemoveProduct(command);
                                clearIt();
                                break;
                            case "8":
                                Execution.SeeSummary(command);
                                clearIt();
                                break;
                            case "9":
                                Execution.PlaceOrder(command);
                                clearIt();
                                break;
                            case "4":
                                loop = 0;
                                break;
                            case "":
                                throw new FormatException("Please enter 1,2,3,4,5,6,7 or 8 only");
                            default:
                                throw new MyException("Please enter 1,2,3,4,5,6,7 or 8 only");
                        }

                        flag = Execution.CheckIfCartEmpty(command);

                    }
                    catch (MyException e)
                    {
                        Console.WriteLine(e.Message);
                        clearIt();
                    }
                    catch (FormatException f)
                    {
                        Console.WriteLine(f.Message);
                        clearIt();
                    }

                    watch.Stop();
                    var Etime = watch.ElapsedMilliseconds;
                    Console.WriteLine("Time taken is : {0} seconds",(Etime/1000));
                }
                sql.Close();
            }
        }
        
        // Clear Console after every option execution

        static void clearIt()
        {
            Console.WriteLine("\nEnter any key to continue");
            Console.ReadLine();
            Console.Clear();
        }

        // Print menu after checking flag returned by CheckIfCartEmpty()

        static void menu(int flag)
        {
            Console.WriteLine("1. See List of Products");
            Console.WriteLine("2. See Category wise List of Products");
            Console.WriteLine("3. Add Product to Cart");
            Console.WriteLine("4. Exit Menu");

            if (flag == 1)
            {
                Console.WriteLine("5. See Cart Details");
                Console.WriteLine("6. Add Quantity of existing Product");
                Console.WriteLine("7. Remove Product from Cart");
                Console.WriteLine("8. See summary of Cart");
                Console.WriteLine("9. Place Order");
            }

            Console.WriteLine("Enter your choice");
            Console.WriteLine("************************************************************************************");

        }
        
    }

}
