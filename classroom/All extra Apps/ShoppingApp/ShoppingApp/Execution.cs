﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    public class Execution
    {
        public static int flag;

        // Returns flag 0 if Cart is empty and 1 otherwise

        public static int CheckIfCartEmpty(SqlCommand command)
        {
            command.CommandText = "select count(*) from Cart";

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                if (Convert.ToInt32(reader[0]) <= 0)
                    flag = 0;
                else
                    flag = 1;
            }
            reader.Close();

            return flag;
        }

        // Prints all available Product details

        public static void ShowAllProducts(SqlCommand command)
        {
            command.CommandText = "select * from Product";
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows == false)
                throw new MyException("No Product available\n");

            while (reader.Read())
            {
                Console.WriteLine(string.Format("ID: {0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice: {4}", reader["Id"], reader["name"], reader["description"], reader["category"], reader["price"]));
                Console.WriteLine();
            }

            reader.Close();
        }

        // Add Product command basic

        public static void AddProductBasic(SqlCommand command, SqlConnection conn, int id)
        {
            SqlCommand comm2 = new SqlCommand();

            comm2.CommandType = System.Data.CommandType.Text;
            comm2.Connection = conn;
            comm2.CommandText = "select * from Product where Id = '" + id + "'";

            SqlDataReader reader = comm2.ExecuteReader();

            if (reader.HasRows == false)
                throw new MyException("Invalid Id input. Please retry.\n");
            else
                Console.WriteLine("Inserting...");

            string n = "";
            string d = "";
            string c = "";
            string p = "";
            while (reader.Read())
            {
                n = Convert.ToString(reader["name"]);
                d = Convert.ToString(reader["description"]);
                c = Convert.ToString(reader["category"]);
                p = Convert.ToString(reader["price"]);
                Console.WriteLine();
            }
            reader.Close();

            SqlCommand c2 = new SqlCommand();
            c2.CommandType = System.Data.CommandType.Text;
            c2.Connection = conn;

            c2.CommandText = "If not exists (select name from Cart where name = '" + n + "') insert into Cart(name,description,category,price,quantity,subtotal) Values('" + n + "','" + d + "','" + c + "','" + p + "','" + Convert.ToString(1) + "','" + p + "')";
            var result = c2.ExecuteNonQuery();

            // If Product already exists, prompt user to add quantity instead

            if (result < 0)
                throw new MyException("Product already exists in Cart. Add quantity instead\n");
            else
            {
                flag = 1;
                Console.WriteLine("Inserted successfully!");
            }
        }

        // Prints Product details category-wise adds to Cart on user choice

        public static void Categorywise(SqlCommand command, SqlConnection sql)
        {
            command.CommandText = "select distinct category from Product";
            SqlDataReader re1 = command.ExecuteReader();
            int i = 0;
            string[] categories = new string[10];
            while (re1.Read())
            {
                categories[i] = Convert.ToString(re1["category"]);
                Console.WriteLine(string.Format("{0}. Category: {1}", i + 1, re1["category"]));
                i += 1;
            }
            Console.WriteLine("Enter choice");
            re1.Close();

            int ch = Convert.ToInt32(Console.ReadLine());

            if (ch > (categories.Length - 1) || ch < 0)
                throw new MyException("Invalid choice entered. Please retry.");

            command.CommandText = "select * from Product where category='" + categories[ch - 1] + "'";

            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows == false)
                throw new MyException("No Product available in this category\n");

            while (reader.Read())
            {
                Console.WriteLine(string.Format("ID: {0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice: {4}", reader["Id"], reader["name"], reader["description"], reader["category"], reader["price"]));
                Console.WriteLine();
            }

            reader.Close();

            // Ask User if they wish to add a product and execute accordingly

            Console.WriteLine("Do you want to add product? Enter 'y' or 'Y' for yes and 'n' or 'N' for no");
            string ans = Console.ReadLine();

            if (ans == "n" || ans == "N")
                return;
            else if (ans == "y" || ans == "Y")
                Console.WriteLine("Choose Id of product");
            else
                throw new MyException("Invalid Id input. Please retry.");

            //adding product

            AddProductBasic(command, sql,Convert.ToInt32(Console.ReadLine()));

        }

        //Add a Product from all products available

        public static void AddProduct(SqlCommand command, SqlConnection conn)
        {
            // Show all available products

            ShowAllProducts(command);
            Console.WriteLine();
            Console.WriteLine("Choose Id of product");

            AddProductBasic(command, conn, Convert.ToInt32(Console.ReadLine()));
        }

        // Show all detailed contents of Cart

        public static void ShowCartDetails(SqlCommand command)
        {
            command.CommandText = "select * from Cart";

            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows == false)
                throw new MyException("No Cart item available\n");

            while (reader.Read())
            {
                Console.WriteLine(string.Format("ID: {0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice: {4}\tQuantity: {5}\tSubtotal {6}", reader["Id"], reader["name"], reader["description"], reader["category"], reader["price"], reader["quantity"], reader["subtotal"]));
                Console.WriteLine();
            }

            reader.Close();
        }

        //Add Quantity to an existing Product in Cart

        public static void AddQuantity(SqlCommand command)
        {
            // Print details of available products in Cart
            ShowCartDetails(command);
            Console.WriteLine("Enter id of product to increase quantity");
            string i = Console.ReadLine();

            string commandstr = "select * from Cart where id =" + i;
            command.CommandText = commandstr;

            SqlDataReader re = command.ExecuteReader();
            int q = 0;
            double p = 0.0;
            double finalq = 1;
            while (re.Read())
            {
                q = Convert.ToInt32(re["quantity"]);
                int id = Convert.ToInt32(re["Id"]);
                p = Convert.ToDouble(re["price"]);
                finalq *= (p * (q + 1));
                q += 1;

            }
            re.Close();

            commandstr = "update Cart set quantity = '" + Convert.ToString(q) + "', subtotal = '" + Convert.ToString(finalq) + "' where Id=" + i;
            command.CommandText = commandstr;

            var result = command.ExecuteNonQuery();

            if (result < 0)
                throw new MyException("Could not Update\n");
            else
                Console.WriteLine("Updated!");

        }

        public static void RemoveProduct(SqlCommand command)
        {
            int res = CheckIfCartEmpty(command);
            if (res == 0)
            {
                throw new MyException("Cart is Empty!\n");
            }

            ShowCartDetails(command);

            Console.WriteLine("Enter id of product to delete from Cart");
            string i = Console.ReadLine();

            string commandstr = "delete from Cart where Id=" + i;
            command.CommandText = commandstr;

            var result = command.ExecuteNonQuery();

            if (result < 0)
                throw new MyException("Invalid input. Please retry.\n");
            else
                Console.WriteLine("Deleted!");

            CheckIfCartEmpty(command);
        }

        // View Total items and Total cost of all products in Cart

        public static void SeeSummary(SqlCommand command)
        {
            // Sum of quantity of each product

            string commandstr = "select sum(quantity) from Cart";
            command.CommandText = commandstr;

            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows == false)
                throw new MyException("No Cart item available\n");

            while (reader.Read())
            {
                Console.WriteLine("No. of items: {0}", Convert.ToString(reader[0]));
                Console.WriteLine();
            }

            reader.Close();

            // Sum of subtotal of each product

            commandstr = "select sum(subtotal) from Cart";
            command.CommandText = commandstr;

            SqlDataReader re = command.ExecuteReader();

            if (re.HasRows == false)
                throw new MyException("No Cart item available\n");

            while (re.Read())
            {
                Console.WriteLine("Total amount: {0}", Convert.ToString(re[0]));
                Console.WriteLine();
            }
            re.Close();
        }

        //Place an order and delete all items from Cart subsequently

        public static void PlaceOrder(SqlCommand command)
        {
            SeeSummary(command);
            Console.WriteLine("Amount is paid!");

            command.CommandText = "delete from Cart";
            var result = command.ExecuteNonQuery();

            if (result < 0)
            {
                throw new MyException("Cannot delete");
            }
            else
            {
                Console.WriteLine("Order Placed!");
                Console.WriteLine("Thankyou for shopping from BLAHBLAH Shopping Center!");
                flag = 0;
            }
        }
    }
}
