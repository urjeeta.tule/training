﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraApplication
{
    class Program
    {
        static List<Department> deps = new List<Department>();
        static int dep_id = 0;

        static List<Employee> emps = new List<Employee>();
        static int emp_id = 0;

        static void Main(string[] args)
        {
            
            int loop = 1;
            while(loop>0)
            {
                menu();
                int ch = Convert.ToInt32(Console.ReadLine());

                switch (ch)
                {
                    case 1:
                        add(emps);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 2:
                        readall(emps);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("Enter id to read");
                        int id = Convert.ToInt32(Console.ReadLine());
                        read(emps,id);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 4:
                        Console.WriteLine("Enter id to update");
                        id =Convert.ToInt32(Console.ReadLine());
                        update(emps, id);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 5:
                        Console.WriteLine("Enter id to delete");
                        id = Convert.ToInt32(Console.ReadLine());
                        delete(emps, id);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 6:
                        deleteAll(emps);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 7:
                        add(deps);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 8:
                        readall(deps);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 9:
                        Console.WriteLine("Enter id to read");
                        id = Convert.ToInt32(Console.ReadLine());
                        read(deps, id);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 10:
                        Console.WriteLine("Enter id to update");
                        id = Convert.ToInt32(Console.ReadLine());
                        update(deps, id);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 11:
                        Console.WriteLine("Enter id to delete");
                        id = Convert.ToInt32(Console.ReadLine());
                        delete(deps, id);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 12:
                        deleteAll(deps);
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 13:
                        loop = 0;
                        break;
                    default:
                        Console.WriteLine("Please enter correct choice number");
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                }
            }
        }

        private static void menu()
        {
            Console.WriteLine("1. Create employee");
            Console.WriteLine("2. Read all employees");
            Console.WriteLine("3. Read specific employee");
            Console.WriteLine("4. Update specific employee");
            Console.WriteLine("5. Delete specific employee");
            Console.WriteLine("6. Delete all employees");
            Console.WriteLine("7. Create department");
            Console.WriteLine("8. Read all departments");
            Console.WriteLine("9. Read specific departments");
            Console.WriteLine("10. Update specific department");
            Console.WriteLine("11. Delete specific department");
            Console.WriteLine("12. Delete all departments");
            Console.WriteLine("13. Exit menu");
            Console.WriteLine("*****************************************************************************************************************************");
        }

        private static void deleteAll(List<Department> deps)
        {
            int j = deps.Count;
            while (j > 0)
            {
                foreach (var item in emps)
                {
                    if (item.depID() == j)
                    {
                        Console.WriteLine("Please delete all employees of Department ID: {0} ", j);
                        return;
                    }
                }

                deps.RemoveAt(j-1);
                j-=1;
            }
            }
        

        private static void delete(List<Department> deps, int id)
        {
            foreach (var item in emps)
            {
                if (item.depID() == id)
                {
                    Console.WriteLine("Please delete all employees of Department ID: {0} ", id);
                    return;
                }
            }

            deps.RemoveAt(id - 1);

        }

        private static void update(List<Department> deps, int id)
        {
            if(deps.Count == 0 || id > deps.Count || id < deps.Count)
            {
                Console.WriteLine("Entry {0} doesn't exist", id);
            }
            else
            {
                Console.WriteLine("Enter name, location of department");
                string[] ans = Console.ReadLine().Split(' ');
                deps[id - 1] = new Department((id), ans[0], ans[1]);
            }
        }



        private static void read(List<Department> deps, int id)
        {
            if (deps.Count == 0)
            {
                Console.WriteLine("No departments available");
            }
            else
            {
                deps[(id - 1)].Print();
                Console.WriteLine("***********************");
            }

        }

        private static void update(List<Employee> emps, int id)
        {
            if (emps.Count == 0 || id > emps.Count || id < emps.Count)
            {
                Console.WriteLine("Entry {0} doesn't exist", id);
            }
            else
            {
                Console.WriteLine("Enter name, designation, basic salary, department id of employee");
                string[] ans = Console.ReadLine().Split(' ');
                emps[id - 1] = new Employee((id), ans[0], ans[1], Convert.ToDouble(ans[2]), Convert.ToInt32(ans[3]));
            }
        }

        private static void deleteAll(List<Employee> emps)
        {
            emps.Clear();
        }

        private static void delete(List<Employee> emps, int id)
        {
            emps.RemoveAt(id - 1);
        }

        private static void read(List<Employee> emps, int id)
        {
            if (emps.Count == 0)
            {
                Console.WriteLine("No employees available");
            }
            else
            {
                emps[(id - 1)].showDets();
                Console.WriteLine("***********************");
            }
        }

        private static void readall(List<Employee> emps)
        {
            if (emps.Count == 0)
            {
                Console.WriteLine("No employees available");
            }
            else
            {
                foreach (var item in emps)
                {
                    item.showDets();
                    Console.WriteLine("*********************************");
                }
            }
        }

        private static void readall(List<Department> deps)
        {
            if (deps.Count == 0)
            {
                Console.WriteLine("No departments available");
            }
            else
            {
                foreach (var item in deps)
                {
                    item.Print();
                    Console.WriteLine("*********************************");
                }
            }
        }

        private static void add(List<Employee> emps)
        {
            Console.WriteLine("Enter name, designation, basic salary, department id of employee");
            string[] ans = Console.ReadLine().Split(' ');
            int di = Convert.ToInt32(ans[3]);
            int f = 0;
            foreach (var item in deps)
            {
                if (di == item.dep_id)
                { f = 1; }
            }
            if(f==0)
            {
                Console.WriteLine("Department doesn't exist. Please create department first.");
            }
            else
            {
                emps.Add(new Employee((emp_id + 1), ans[0], ans[1], Convert.ToDouble(ans[2]), Convert.ToInt32(ans[3])));
                emp_id += 1;

            }
        }

        private static void add(List<Department> deps)
        {
            Console.WriteLine("Enter name, location of department");
            string[] ans = Console.ReadLine().Split(' ');
            deps.Add(new Department((dep_id + 1), ans[0], ans[1]));
            dep_id += 1;
        }
    }
}
