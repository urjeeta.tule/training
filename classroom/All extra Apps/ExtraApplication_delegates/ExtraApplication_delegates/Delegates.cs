﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraApplication_delegates
{
    public delegate void AE(List<Employee> e, List<Department> d);
    public delegate void RAD(List<Department> d);
    public delegate void RUDE(List<Employee> e, int i);
    public delegate void RUD(List<Department> d, int i);
    public delegate void DD(List<Department> d, List<Employee> e, int i);
    public delegate void RE(List<Employee> e);


    public class Delegates
    {
        int emp_id = 0;

        public event AE addEM;
        public event RAD addDP;

        public event RE readAllEM;
        public event RAD readAllDP;

        public event RUD readDP;
        public event RUD updateDP;

        public event DD deleteDP;

        public event RUDE readEM;
        public event RUDE updateEM;
        public event RUDE deleteEM;

        public event RE deleteAllEM;
        public event AE deleteAllDP;
        

        public void RaiseAdd(List<Employee> e, List<Department> d)
        {
            addEM(e, d);
        }

        public void RaiseAddD(List<Department> d)
        {
            addDP(d);
        }

        public void RaiseReadAllE(List<Employee> e)
        {
            readAllEM(e);
        }
        
        public void RaiseReadAllD(List<Department> d)
        {
            readAllDP(d);
        }
        public void RaiseReadD(List<Department> d,int i)
        {
            readDP(d, i);
        }

        public void RaiseUpdateD(List<Department> d,int i)
        {
            updateDP(d, i);
        }

        public void RaiseDeleteD(List<Department> d,List<Employee> e, int i)
        {
            deleteDP(d, e, i);
        }

        public void RaiseReadE(List<Employee>e, int i)
        {
            readEM(e, i);
        }

        public void RaiseDeleteAllD(List<Employee> e, List<Department> d)
        {
            deleteAllDP(e, d);
        }

        public void RaiseUpdateE(List<Employee>e,int i)
        {
            updateEM(e, i);
        }

        public void RaiseDeleteE(List<Employee>e,int i)
        {
            deleteEM(e, i);
        }

        public void RaiseDeleteAllE(List<Employee> e)
        {
            deleteAllEM(e);
        }
    }
}
