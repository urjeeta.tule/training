﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraApplication_delegates
{
    
    public class Program
    {
        static List<Department> deps = new List<Department>();
        static int dep_id = 0;

        static List<Employee> emps = new List<Employee>();
        static int emp_id = 0;

        static void Main(string[] args)
        {

            int loop = 1;
            while (loop > 0)
            {
                menu();
                int ch = Convert.ToInt32(Console.ReadLine());

                Delegates d = new Delegates();
                
                switch (ch)
                {
                    case 1:
                        d.addEM += addit;
                        d.RaiseAdd(emps, deps);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 2:
                        d.readAllEM += readall;
                        d.RaiseReadAllE(emps);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("Enter id to read");
                        int id = Convert.ToInt32(Console.ReadLine());
                        d.readEM += read;
                        d.RaiseReadE(emps, id);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 4:
                        Console.WriteLine("Enter id to update");
                        id = Convert.ToInt32(Console.ReadLine());
                        d.updateEM += update;
                        d.RaiseUpdateE(emps, id);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 5:
                        Console.WriteLine("Enter id to delete");
                        id = Convert.ToInt32(Console.ReadLine());
                        d.deleteEM += delete;
                        d.RaiseDeleteE(emps, id);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 6:
                        d.deleteAllEM += deleteAll;
                        d.RaiseDeleteAllE(emps);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 7:
                        d.addDP += add;
                        d.RaiseAddD(deps);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 8:
                        d.readAllDP += readall;
                        d.RaiseReadAllD(deps);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 9:
                        Console.WriteLine("Enter id to read");
                        id = Convert.ToInt32(Console.ReadLine());

                        d.readDP += read;
                        d.RaiseReadD(deps, id);
                        
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 10:
                        Console.WriteLine("Enter id to update");
                        id = Convert.ToInt32(Console.ReadLine());

                        d.updateDP += update;
                        d.RaiseUpdateD(deps, id);
                        
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 11:
                        Console.WriteLine("Enter id to delete");
                        id = Convert.ToInt32(Console.ReadLine());
                        d.deleteDP += delete;
                        d.RaiseDeleteD(deps, emps, id);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 12:
                        d.deleteAllDP += deleteAll;
                        d.RaiseDeleteAllD(emps, deps);

                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 13:
                        loop = 0;
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Please enter correct choice number");
                        Console.WriteLine();
                        Console.WriteLine("Press Enter to continue");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                }
            }
        }

        public static void menu()
        {
            Console.WriteLine("1. Create employee");
            Console.WriteLine("2. Read all employees");
            Console.WriteLine("3. Read specific employee");
            Console.WriteLine("4. Update specific employee");
            Console.WriteLine("5. Delete specific employee");
            Console.WriteLine("6. Delete all employees");
            Console.WriteLine("7. Create department");
            Console.WriteLine("8. Read all departments");
            Console.WriteLine("9. Read specific departments");
            Console.WriteLine("10. Update specific department");
            Console.WriteLine("11. Delete specific department");
            Console.WriteLine("12. Delete all departments");
            Console.WriteLine("13. Exit menu");
        }
        

        //UPDATE

        public static void update(List<Department> deps, int id)
        {
            if (deps.Count == 0 || id > deps.Count || id < deps.Count)
            {
                Console.WriteLine("Entry {0} doesn't exist", id);
            }
            else
            {
                Console.WriteLine("Enter name, location of department");
                string[] ans = Console.ReadLine().Split(' ');
                deps[id - 1] = new Department((id), ans[0], ans[1]);
            }
        }
        
        public static void update(List<Employee> emps, int id)
        {
            if (emps.Count == 0 || id > emps.Count || id < emps.Count)
            {
                Console.WriteLine("Entry {0} doesn't exist", id);
            }
            else
            {
                Console.WriteLine("Enter name, designation, basic salary, department id of employee");
                string[] ans = Console.ReadLine().Split(' ');
                emps[id - 1] = new Employee(ans[0], ans[1], Convert.ToDouble(ans[2]), Convert.ToInt32(ans[3]));
            }
        }

        //DELETE ONE

        public static void delete(List<Department> deps, List<Employee> emps, int id)
        {
            foreach (var item in emps)
            {
                if (item.depID() == id)
                {
                    Console.WriteLine("Please delete all employees of Department ID: {0} ", id);
                    return;
                }
            }

            deps.RemoveAt(id - 1);

        }

        public static void delete(List<Employee> emps, int id)
        {
            emps.RemoveAt(id - 1);
        }

        //READ ONE

        public static void read(List<Department> deps, int id)
        {
            if (deps.Count == 0)
            {
                Console.WriteLine("No departments available");
            }
            else
            {
                deps[(id - 1)].Print();
                Console.WriteLine("***********************");
            }

        }


        public static void read(List<Employee> emps, int id)
        {
            if (emps.Count == 0)
            {
                Console.WriteLine("No employees available");
            }
            else
            {
                emps[(id - 1)].showDets();
                Console.WriteLine("***********************");
            }
        }

        //DELETE ALL

        public static void deleteAll(List<Employee> emps, List<Department> deps)
        {
            int j = deps.Count;
            while (j > 0)
            {
                foreach (var item in emps)
                {
                    if (item.depID() == j)
                    {
                        Console.WriteLine("Please delete all employees of Department ID: {0} ", j);
                        return;
                    }
                }

                deps.RemoveAt(j - 1);
                j -= 1;
            }
        }

        public static void deleteAll(List<Employee> emps)
        {
            emps.Clear();
        }

        

        //READALL

        public static void readall(List<Department> deps)
        {
            if (deps.Count == 0)
            {
                Console.WriteLine("No departments available");
            }
            else
            {
                foreach (var item in deps)
                {
                    item.Print();
                    Console.WriteLine("*********************************");
                }
            }
        }

        public static void readall(List<Employee> emps)
        {
            if (emps.Count == 0)
            {
                Console.WriteLine("No employees available");
            }
            else
            {
                foreach (var item in emps)
                {
                    item.showDets();
                    Console.WriteLine("*********************************");
                }
            }
        }

        

        //ADD


        public static void add(List<Department> deps)
        {
            Console.WriteLine("Enter name, location of department");
            string[] ans = Console.ReadLine().Split(' ');
            deps.Add(new Department((dep_id + 1), ans[0], ans[1]));
            dep_id += 1;
        }

        public static void addit(List<Employee> em, List<Department> de)
        {
            Console.WriteLine("Enter name, designation, basic salary, department id of employee");
            string[] ans = Console.ReadLine().Split(' ');
            int di = Convert.ToInt32(ans[3]);
            int f = 0;
            foreach (var item in de)
            {
                if (di == item.dep_id)
                { f = 1; }
            }
            if (f == 0)
            {
                Console.WriteLine("Department doesn't exist. Please create department first.");
            }
            else
            {
                em.Add(new Employee(ans[0], ans[1], Convert.ToDouble(ans[2]), Convert.ToInt32(ans[3])));
                emp_id++;
            }
        }
    }
}
