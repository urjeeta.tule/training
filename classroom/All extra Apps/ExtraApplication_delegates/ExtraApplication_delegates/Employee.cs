﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraApplication_delegates
{
    public class Employee
    {
        public static int e_id = 0;
        public string name;
        public string desig;
        public double basic;
        public double hra;
        public double pf;
        public double grs;
        public double netsal;
        public int dep_id;

        public Employee()
        {
            e_id++;
            name = "";
            desig = "";
            basic = -99.99;
            hra = -99.99;
            pf = -99.99;
            grs = -99.99;
            netsal = -99.99;
            dep_id = -9999;

        }

        public Employee(string n, string d, double b, int di)
        {
            e_id++;
            name = n;
            desig = d;
            basic = b;
            dep_id = di;
        }

        public void getnet()
        {
            pf = 0.12 * basic;
            hra = 0.08 * basic;
            grs = basic + hra + 15000.0;
            netsal = grs - (2500.0 + pf);
        }

        public int depID()
        { return dep_id; }

        public void showDets()
        {
            getnet();
            Console.WriteLine("Details of Employee: ");
            Console.WriteLine("Employee ID:\t" + e_id);
            Console.WriteLine("Name:\t" + name);
            Console.WriteLine("Designation:\t" + desig);
            Console.WriteLine("Basic:\t" + basic);
            Console.WriteLine("HRA:\t" + hra);
            Console.WriteLine("PF:\t" + pf);
            Console.WriteLine("Gross Salary:\t" + grs);
            Console.WriteLine("Net Salary:\t" + netsal);
            Console.WriteLine("Department ID:\t" + dep_id);
        }
    }
}
