﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SerializationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //create book.txt to store all book information
            FileStream fs = File.OpenWrite(@"C:\training\books.txt");
            //FileStream fs2 = File.OpenWrite(@"C:\training\books.txt");


            //create formatter for storing books in books.txt
            BinaryFormatter bf = new BinaryFormatter();

            //Book b1 = new Book { BookID = 100, BookName = "Intro to C#", Price = 245 };
            //b1.SetPublisher("Packet");

            //Book b2 = new Book { BookID = 101, BookName = "Pro C Sharp", Price = 550 };
//            b2.SetPublisher("APress");

            List<Book> bs = new List<Book>(10);
            bs.Add(new Book { BookID = 100, BookName = "Intro to C#", Price = 245 });
            bs[0].SetPublisher("Packet");
            bs.Add(new Book { BookID = 101, BookName = "Pro C Sharp", Price = 550 });
            bs[1].SetPublisher("APress");
            //bf.Serialize(fs,b1);
            //bf.Serialize(fs2, b2);
            bf.Serialize(fs, bs);
            fs.Close();
            //fs2.Close();

            Console.WriteLine("Books stored in Books.txt file!");

            fs = File.OpenRead(@"C:\training\books.txt");
            //fs2 = File.OpenRead(@"C:\training\books.txt");
            List<Book> bsnew =(List<Book>) bf.Deserialize(fs);
            fs.Close();
            //fs2.Close();

            Book b = bsnew[0];
            Book b3 = bsnew[1];

            Console.WriteLine(b);
            Console.WriteLine(b3);
            Console.ReadLine();
        }
    }
}
