﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTOInMemoObjects
{
    class Customer
    {
        public string CustomerID{ get; set; }
        public string City { get; set; }

        public override string ToString()
        {
            return CustomerID+"\t"+City;
        }
    }
}
