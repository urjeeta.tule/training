﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqTOInMemoObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            //NumQuery();
            //ObjectQuery();
            XMLQuery();
            CreateXMLfromProduct();
            Console.ReadLine();
        }
        static void NumQuery()
        {
            var numbers = new int[] { 1, 4, 9, 16, 25, 36 };
            var evenNumbers = from p in numbers
                              where (p % 2 == 0)
                              select p;
            Console.WriteLine("Result:");
            foreach (var item in evenNumbers)
            {
                Console.WriteLine(item);
            }
        }

        static IEnumerable<Customer> CreateCustomers()
        {

            return from c in XDocument.Load("Customers.xml").Descendants("Customers").Descendants()
                   select new Customer { City = c.Attribute("City").Value,
                       CustomerID = c.Attribute("CustomerID").Value };

            //return new List<Customer>
            //{
            //    new Customer{CustomerID = "ALFKI", City="Berlin"},
            //    new C]]]]]]]ustomer{CustomerID = "BONAP", City="Marsielle"},
            //    new Customer{CustomerID = "CONSH", City="London"},
            //    new Customer{CustomerID = "EASTC", City="London"},
            //    new Customer{CustomerID = "FRANS", City="Torino"},
            //    new Customer{CustomerID = "LONEP", City="Portland"},
            //    new Customer{CustomerID = "NORTS", City="London"},
            //    new Customer{CustomerID = "THEBI", City="Portland"}

            //};

        }

        static void ObjectQuery()
        {
            var c = from cu in CreateCustomers()
                    where cu.City == "London"
                    select cu;
            foreach (var cu in c)
            {
                Console.WriteLine(cu.ToString());
            }
        }

        static void XMLQuery()
        {
            var doc = XDocument.Load("Customers.xml");

            var result = from c in doc.Descendants("Customer")
                         where c.Attribute("City").Value == "London"
                         select c;

            XElement transformdoc = new XElement("Londoners",
                                    from customer in result
                                    select new XElement("Contact",
                                    new XAttribute("City", customer.Attribute("City").Value),
                                    new XAttribute("ID", customer.Attribute("CustomerID").Value),
                                    new XAttribute("Name",customer.Attribute("ContactName").Value)));

            transformdoc.Save("Londoners.xml");

            Console.WriteLine("Result: \n{0}",transformdoc);
            //foreach (var item in result)
            //{
            //    Console.WriteLine("{0} \n",item);
            //}
        }

        static void CreateXMLfromProduct()
        {
            XElement P = new XElement("Products",
                           new XElement("Product", new XAttribute("ID", "1"),
                           new XElement("Name", "Football"),
                           new XElement("Price", "34.5")),
                           new XElement("Product", new XAttribute("ID", "2"),
                           new XElement("Name", "Bat"),
                           new XElement("Price", "14.5")));

            Console.WriteLine(P);

            P.Save("Products.xml");

        }

    }

    
}
