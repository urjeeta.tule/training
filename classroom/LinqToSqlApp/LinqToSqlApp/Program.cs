﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToSqlApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ObjectQuery();

            Console.ReadLine();
        }

        static void ObjectQuery()
        {
            var db = new TrainingDBDataClassesDataContext();
            db.Log = Console.Out;

            var result = from p in db.Products
                         where p.Price > 30
                         select p;

            foreach (var item in result)
            {
                Console.WriteLine("ID: {0}\tName: {1}\t",item.Id,item.Name);
            }
        }
    }
}
