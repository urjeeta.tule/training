﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            int loop = 1;
            while (loop > 0)
            {
                Console.WriteLine("1. Employee");
                Console.WriteLine("2. Date");
                Console.WriteLine("3. Shape");
                Console.WriteLine("4. Exit");

                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        Console.WriteLine("Please enter Employee ID, Name, Designation, Basic Salary and Department ID.");
                        string[] ans = Console.ReadLine().Split(' ');
                        Employee e = new Employee(Convert.ToInt32(ans[0]), ans[1], ans[2], Convert.ToDouble(ans[3]), Convert.ToInt32(ans[4]));
                        break;
                    case 2:
                        Console.WriteLine("Please enter Date, Month, Year.");
                        ans = Console.ReadLine().Split(' ');
                        Date d = new Date(ans[0],ans[1],ans[2]);
                        d.Print();
                        break;
                    case 3:
                        Shape c = new Shape();
                        c.Print();
                        break;
                    case 4:
                        loop = 0;
                        break;
                    default:
                        Console.WriteLine("Please enter 1,2,3 or 4 as choice only");
                        break;
                }
            }
        }
    }
}
