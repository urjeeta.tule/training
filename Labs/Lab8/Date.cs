﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public class Date : IPrintable
    {
        public string mDay, mMonth, mYear;

        public Date()
        {
            mDay = "1"; mMonth = "1"; mYear = "1987";
        }

        public Date(string d, string m, string y)
        {
            mDay = d; mMonth = m; mYear = y;
        }

        public void Print()
        {
            Console.WriteLine(mDay + "-" + mMonth + "-" + mYear);
        }
    }
}
