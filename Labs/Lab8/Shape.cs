﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public class Shape : IPrintable
    {
        public double Area()
        {
            Console.WriteLine("Enter radius of Circle");
            double r = Convert.ToDouble(Console.ReadLine());
            return 3.14 * r * r;
        }

        public void Print()
        {
            Console.WriteLine("Area of Circle:\t"+Area());
        }
    }
}
