﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Payroll
{
    public class cntmnt
    {
        Employee e;
        string doj;

        public cntmnt(int i, string n, string d, double b, int di, string da)
        {
            e = new Employee(i, n, d, b, di);
            doj = da;
        }

        public cntmnt()
        {
            e = new Employee();
            doj = "";
        }

        public void display()
        {
            e.getnet();
            e.showDets();
            Console.WriteLine("Date of Joining:\t"+doj);
        }

    }
}
