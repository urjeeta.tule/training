﻿namespace Dates
{
    public class Date
    {
        public string mDay, mMonth, mYear;

        public Date()
        {
            mDay = "1"; mMonth = "1"; mYear = "1987";
        }

        public Date(string d, string m, string y)
        {
            mDay = d; mMonth = m; mYear = y;
        }

        public void printdate()
        {
            System.Console.WriteLine(mDay + "-" + mMonth + "-" + mYear);
        }
    }


}
