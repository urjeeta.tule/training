﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            int loop = 1;
            while (loop > 0)
            {
                Console.WriteLine("Please enter choice:");
                Console.WriteLine("1. Employee details");
                Console.WriteLine("2. Exit");
                int ch = Convert.ToInt32(Console.ReadLine());

                switch (ch)
                {
                    case 1:
                        Console.WriteLine("Please enter Employee ID, Name, Designation, Basic Salary and Department ID.");
                        string[] ans = Console.ReadLine().Split(' ');
                        Employee e = new Employee(Convert.ToInt32(ans[0]), ans[1], ans[2], Convert.ToDouble(ans[3]), Convert.ToInt32(ans[4]));

                        break;
                    case 2:
                        loop = 0;
                        break;
                    default:
                        Console.WriteLine("Please enter 1 or 2 as choice");
                        break;
                }
            }    
            Console.WriteLine("Count from variable:\t"+Convert.ToString(Employee.count));
            Console.WriteLine(Employee.TotalCount());
        }
    }
}
