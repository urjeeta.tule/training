﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            int loop = 1;
            while (loop > 0)
            {
                Console.WriteLine("1. Circle");
                Console.WriteLine("2. Rectangle");
                Console.WriteLine("3. Shape");
                Console.WriteLine("4. Quit");
                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        Circle c = new Circle();
                        Console.WriteLine(c.Area());
                        break;
                    case 2:
                        Rectangle r = new Rectangle();
                        Console.WriteLine(r.Area());
                        break;
                    case 3:
                        Shape s = new Circle();
                        Console.WriteLine(s.Area());
                        Shape s1 = new Rectangle();
                        Console.WriteLine(s1.Area());
                        break;
                    case 4:
                        loop = 0;
                        break;
                    default:
                        Console.WriteLine("Please enter 1,2 or 3 only");
                        break;
                }
            }
        }
    }
}
