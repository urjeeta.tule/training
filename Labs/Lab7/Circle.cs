﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    public class Circle : Shape
    {
        public override double Area()
        {
            Console.WriteLine("Enter radius of Circle");
            double r = Convert.ToDouble(Console.ReadLine());
            return 3.14*r*r;
        }
    }
}
