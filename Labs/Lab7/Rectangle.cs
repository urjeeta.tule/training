﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    public class Rectangle : Shape
    {
        public override double Area()
        {
            Console.WriteLine("Enter length and breadth of Rectangle");
            string[] ans = Console.ReadLine().Split(' ');
            double l = Convert.ToDouble(ans[0]);
            double b = Convert.ToDouble(ans[1]);
            return l*b;
        }
    }
}
