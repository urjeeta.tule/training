﻿using Dates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Lab2
    {
        static void Main(string[] args)
        {
            Date dt1 = new Date { mDay = args[0], mMonth = args[1], mYear = args[2] };
            dt1.printdate();

            Date dt2 = new Date();
            dt2.printdate();
        }
        
    }
}
