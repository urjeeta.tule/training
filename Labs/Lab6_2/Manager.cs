﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_2
{
    public class Manager : Employee
    {
        double pa;
        double fa;
        double oa;

        public Manager() : base()
        {
            pa = 0.0;
            fa = 0.0;
            oa = 0.0;
        }

        public Manager(int e_id, string n, double b) : base(e_id, n, b)
        {
            pa = 0.08 * b;
            fa = 0.13 * b;
            oa = 0.03 * b;
        }

        public override string ToString()
        {
            return base.ToString()+"\t" + pa + "\t" + fa + "\t" + oa; 
        }


    }
}
