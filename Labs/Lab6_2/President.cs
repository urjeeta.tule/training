﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_2
{
    public class President : Employee
    {
        int e_id;
        string name;
        double km;
        double toa;
        double ta = 2000.0;
        double basic;
        public President(int e_id, string name, double basic, double km): base(e_id, name, basic)
        {
            this.e_id = e_id;
            this.name = name;
            this.basic = basic;
            this.km = km;
            toa = 8.0 * km;
        }

        public override string ToString()
        {
            return base.ToString()+"\t"+km+"\t"+toa+"\t"+ta;
        }

    }

}
