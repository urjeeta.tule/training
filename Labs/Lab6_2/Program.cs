﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter Employee ID, Name, Designation, Basic Salary and Department ID.");
            string[] ans = Console.ReadLine().Split(' ');
            Employee e = new Employee(Convert.ToInt32(ans[0]), ans[1], ans[2], Convert.ToDouble(ans[3]), Convert.ToInt32(ans[4]));

            Console.WriteLine(e.ToString());

            Console.WriteLine("Please enter Manager ID, Name, Basic Salary");
            ans = Console.ReadLine().Split(' ');
            Manager m = new Manager(Convert.ToInt32(ans[0]),ans[1],Convert.ToDouble(ans[2]));
            Console.WriteLine(m.ToString());

            Console.WriteLine("Please enter President ID, Name, Basic Salary, Kilometers");
            ans = Console.ReadLine().Split(' ');
            President p = new President(Convert.ToInt32(ans[0]),ans[1],Convert.ToDouble(ans[2]),Convert.ToDouble(ans[3]));
            Console.WriteLine(p.ToString());

        }
    }
}
