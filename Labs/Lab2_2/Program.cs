﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int loop = 1;
            while (loop > 0)
            {
                Console.WriteLine("Please enter choice:");
                Console.WriteLine("1. Employee");
                Console.WriteLine("2. Department");
                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        Console.WriteLine("Please enter Employee ID, Name, Designation, Basic Salary and Department ID.");
                        string[] ans = Console.ReadLine().Split(' ');
                        Employee e = new Employee(Convert.ToInt32(ans[0]), ans[1], ans[2], Convert.ToDouble(ans[3]), Convert.ToInt32(ans[4]));
                        e.getnet();
                        e.showDets();
                        
                        //
                        break;
                    case 2:
                        Console.WriteLine("Please enter Department ID, Name, Location.");
                        ans = Console.ReadLine().Split(' ');

                        Department d = new Department(Convert.ToInt32(ans[0]), ans[1], ans[2]);
                        d.Print();
                        break;
                    default:
                        Console.WriteLine("Please enter 0 to quit menu");
                        loop = Convert.ToInt32(Console.ReadLine());
                        break;
                }

            }
            

        }
    }
}
