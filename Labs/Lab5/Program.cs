﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter Employee ID, Name, Designation, Basic Salary and Department ID.");
            string[] ans = Console.ReadLine().Split(' ');
            Employee e = new Employee(Convert.ToInt32(ans[0]), ans[1], ans[2], Convert.ToDouble(ans[3]), Convert.ToInt32(ans[4]));
            e.getnet();
            Console.WriteLine(e.ToString());
        }
    }
}
