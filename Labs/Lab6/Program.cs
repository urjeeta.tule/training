﻿using Payroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter Employee ID, Name, Designation, Basic Salary, Department ID and Date of Joining.");
            string[] ans = Console.ReadLine().Split(' ');
            cntmnt e = new cntmnt(Convert.ToInt32(ans[0]), ans[1], ans[2], Convert.ToDouble(ans[3]), Convert.ToInt32(ans[4]),ans[5]);
            e.display();
            Console.ReadLine();
        }
    }
}
