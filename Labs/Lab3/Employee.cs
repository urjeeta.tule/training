﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
        class Employee
        {
            int e_id;
            string name;
            string desig;
            double basic;
            double hra;
            double pf;
            double grs;
            double netsal;
            int dep_id;

            public Employee()
            {
                e_id = -9999;
                name = "";
                desig = "";
                basic = -99.99;
                hra = -99.99;
                pf = -99.99;
                grs = -99.99;
                netsal = -99.99;
                dep_id = -9999;

            }

            public Employee(int i, string n, string d, double b, int di)
            {
                e_id = i;
                name = n;
                desig = d;
                basic = b;
                dep_id = di;
            }

            public string getds()
            { return desig; }
            public void getnet(int e)
            {
            pf = 0.12 * basic;
            hra = 0.08 * basic;
            grs = basic + hra + 15000.0;
            netsal = grs - (2500.0 + pf);
            }

            public void getnet(int e, char ch)
            {
            switch (ch)
            {
                case 'M':
                    pf = 0.12 * basic;
                    hra = 0.08 * basic;
                    grs = basic + hra + 25000.0;
                    netsal = grs - (2500.0 + pf);
                    break;
                case 'P':
                    pf = 0.12 * basic;
                    hra = 0.08 * basic;
                    grs = basic + hra + 45000.0;
                    netsal = grs - (2500.0 + pf);
                    break;
                default:
                    break;
            }
            }

            public void showDets()
            {
                Console.WriteLine("Details of Employee: ");
                Console.WriteLine("Employee ID:\t" + e_id);
                Console.WriteLine("Name:\t" + name);
                Console.WriteLine("Designation:\t" + desig);
                Console.WriteLine("Basic:\t" + basic);
                Console.WriteLine("HRA:\t" + hra);
                Console.WriteLine("PF:\t" + pf);
                Console.WriteLine("Gross Salary:\t" + grs);
                Console.WriteLine("Net Salary:\t" + netsal);
                Console.WriteLine("Department ID:\t" + dep_id);
            }
        }
    }

